/*! New approach to generating fluid vector fields.
 * This time, don't store flow vectors in cells, but only flows between pair of neighboring cells.
 * Divergence will be defined as inflows+outflows from the given cell,
 * and splitting the cell into 4 sub-cells while keeping flows intact will be simple (ambiguity can be constrained too).
 */

use nalgebra::Complex;
use ndarray::Array2;
use std::cmp::Ordering::Equal;


pub type FlowField = Array2<Complex<f32>>;


fn cartesian_iter(xsize: usize, ysize: usize) -> impl Iterator<Item=Index> {
    (0..xsize).flat_map(move |xidx|
        (0..ysize).map(move |yidx|
            Index { x: xidx, y: yidx }
        )
    )
}

#[derive(Debug, PartialEq)]
pub struct Index {
    pub x: usize,
    pub y: usize,
}

impl Index {
    fn next_x(&self) -> Index {
        Index { x: self.x + 1, y: self.y }
    }
    fn next_y(&self) -> Index {
        Index { x: self.x, y: self.y + 1 }
    }
}


/// Surrounded by an inpermeable wall in both axes, on both ends.
#[derive(Debug, PartialEq)]
struct Flows {
    /// Positive value: flow goes from cell of lower x-index to higher.
    /// Contains the canonical sizing.
    x_flows: Array2<f32>,
    /// Positive value: flow goes from cell of lower y-index to higher.
    /// x-size must be 1 greater than that of x_flows, y-size must be 1 less than that of x_flows.
    /// Together, they cover all cell neighbor pairs.
    y_flows: Array2<f32>,
}


impl Flows {
    fn new(shape: (usize, usize)) -> Flows {
        let (x, y) = shape;
        Flows {
            x_flows: Array2::zeros((x - 1, y)),
            y_flows: Array2::zeros((x, y - 1)),
        }
    }

    fn to_vector_field(&self) -> FlowField {
        let get_vector = |index: &Index| {
            let xmag = self.from_lower_x(index) - self.from_higher_x(index);
            let ymag = self.from_lower_y(index) - self.from_higher_y(index);
            Complex::new(xmag, ymag)
        };
        
        let mut field = FlowField::zeros(self.shape());
        for index in self.cells() {
            field[(index.x, index.y)] = get_vector(&index);
        }
        field
    }

    fn shape(&self) -> (usize, usize) {
        let shape = self.x_flows.shape();
        (shape[0] + 1, shape[1])
    }

    fn from_lower_x(&self, index: &Index) -> f32 {
        if index.x == 0 {
            0.0
        } else {
            self.x_flows[(index.x - 1, index.y)]
        }
    }
    
    fn from_higher_x(&self, index: &Index) -> f32 {
        let (xsize, _ysize) = self.shape();
        if index.x == xsize - 1 {
            0.0
        } else {
            -self.x_flows[(index.x, index.y)]
        }
    }

    fn from_lower_y(&self, index: &Index) -> f32 {
        if index.y == 0 {
            0.0
        } else {
            self.y_flows[(index.x, index.y - 1)]
        }
    }
    
    fn from_higher_y(&self, index: &Index) -> f32 {
        let (_xsize, ysize) = self.shape();
        if index.y == ysize - 1 {
            0.0
        } else {
            -self.y_flows[(index.x, index.y)]
        }
    }
    
    fn to_higher_x_mut(&mut self, index: &Index) -> &mut f32 {
        self.x_flows.get_mut((index.x, index.y)).unwrap()
    }

    fn to_higher_y_mut(&mut self, index: &Index) -> &mut f32 {
        self.y_flows.get_mut((index.x, index.y)).unwrap()
    }

    fn divergence_at(&self, index: &Index) -> f32 {
        -(self.from_lower_x(index) + self.from_lower_y(index) + self.from_higher_x(index) + self.from_higher_y(index))
    }

    fn cells(&self) -> impl Iterator<Item=Index> {
        let (xsize, ysize) = self.shape();
        cartesian_iter(xsize, ysize)
    }

    fn x_cells(&self) -> impl Iterator<Item=Index> {
        let shape = self.x_flows.shape();
        let (xsize, ysize) = (shape[0], shape[1]);
        cartesian_iter(xsize, ysize)
    }

    fn y_cells(&self) -> impl Iterator<Item=Index> {
        let shape = self.y_flows.shape();
        let (xsize, ysize) = (shape[0], shape[1]);
        cartesian_iter(xsize, ysize)
    }

    /// Fill in known - copied - walls with the same value.
    /// Flow will increase 2x because of the copying, but it's a matter of convention.
    fn fill_known(&self) -> Flows {
        let (x, y) = self.shape();
        let mut into = Flows::new((x * 2, y * 2));

        for index in self.x_cells() {
            into.x_flows[(index.x * 2 + 1, index.y * 2)] += self.x_flows[(index.x, index.y)];
            into.x_flows[(index.x * 2 + 1, index.y * 2 + 1)] += self.x_flows[(index.x, index.y)];
        }

        for index in self.y_cells() {
            into.y_flows[(index.x * 2, index.y * 2 + 1)] += self.y_flows[(index.x, index.y)];
            into.y_flows[(index.x * 2 + 1, index.y * 2 + 1)] += self.y_flows[(index.x, index.y)];
        }
        into
    }

    /// Surplus is accurate despite including inner walls in the measurement, as long as inner walls start at 0.
    /// Surplus table goes round, starting from a.
    /// ↑ a → b ↓
    /// ↑ d ← c ↓
    fn _surplus_at_square(&self, index: &Index) -> [f32; 4] {
        [
            -self.divergence_at(&index),
            -self.divergence_at(&index.next_x()),
            -self.divergence_at(&index.next_x().next_y()),
            -self.divergence_at(&index.next_y()), 
        ]
    }

    /// Fills in gaps between copied walls. Uncopied walls must be 0.0.
    fn fill_split(&mut self) {
        let (xsize, ysize) = self.shape();
        // First cell in every other row & column.
        let split_cells = cartesian_iter(xsize / 2, ysize / 2)
            .map(|index| Index { x: index.x * 2, y: index.y * 2 });
        for index in split_cells {
            // Insert fancy flow algorithm here.
            // Goal: find flow values through inner walls, knowing flow values through outer walls.
            let surplus = self._surplus_at_square(&index);
            
            // Find highest surplus, work from there until no more surplus.
            let maxidx = surplus.iter()
                .enumerate()
                .max_by(|(_, s), (_, s2)| s.partial_cmp(s2).unwrap_or(Equal))
                .map(|(i, _)| i)
                .unwrap();
            let surplus: Vec<_>
                = surplus.iter().chain(surplus.iter()).chain(surplus.iter())
                    .map(|v| *v)
                    .collect();
            // Now surplus[maxidx] == a, [+1] = b, [+2] = c, [+3] = d,
            // and surplus[0] is at the original square index
            let flows = split_flows(&surplus[maxidx..(maxidx + 4)]);
            // Now flows[0] = a
            let flows = extend(&flows);
            let flows = &flows[(4 - maxidx)..];
            // Now flows[maxidx] = a, and flows[0] is at the original square index
            *self.to_higher_x_mut(&index) += flows[0];
            *self.to_higher_y_mut(&index.next_x()) += flows[1];
            *self.to_higher_x_mut(&index.next_y()) -= flows[2];
            *self.to_higher_y_mut(&index) -= flows[3];
        }
    }

    fn divide(&self) -> Self {
        let mut flows = self.fill_known();
        flows.fill_split();
        flows
    }

    /// Stirs the cell and those with indices greater by 1.
    /// Starts by adding value to the x+1 wall.
    fn stir_at(&mut self, index: &Index, value: f32) {
        // Move to next x
        *self.to_higher_x_mut(index) += value;
        // Move to next y
        *self.to_higher_y_mut(&Index { x: index.x + 1, y: index.y }) += value;
        // Move to previous x
        *self.to_higher_x_mut(&Index { x: index.x, y: index.y + 1 }) -= value;
        // Move to previous y (starting cell)
        *self.to_higher_y_mut(index) -= value;
    }

    fn stir(&mut self, f: impl Fn(&Index) -> f32) {
        let stirrable_cells = self.cells()
            // Can only stir one less row/column than available, cause 2x2 square needed.
            .filter(|index| index.x != 0 && index.y != 0)
            // Stir indexing starts at 0, so move the indexing back one notch.
            .map(|index| Index { x: index.x - 1, y: index.y - 1 });
        for index in stirrable_cells {
            self.stir_at(&index, f(&index));
        }
    } 
}

/// Surplus must be at least 4 items: a, b, c, d. Returns ab, bc, cd, da.
fn split_flows(surplus: &[f32]) -> [f32;4] {
    /// Split surplus into 2 neighboring cells, noting the flow.
    fn split(a: f32, b: f32, d: f32) -> (f32, f32) {
        // a is always the highest, so both flows will be nonnegative.
        let ab = a - b;
        let ad = a - d;
        let base = if ab + ad == 0.0 {
            0.5
        } else {
            a / (ab + ad)
        };
        (ab * base, ad * base)
    };

    let (ab, ad) = split(surplus[0], surplus[1], surplus[3]);
    let b = ab + surplus[1];
    let d = ad + surplus[3];
    let oldc = surplus[2];

    let (cd, cb) = if b * d > 0.0 {
        // c is either the other source or the final sink.
        // Either way, it's the end destination of both flows.
        (-d, -b)
    } else {
        // d still has surplus, and flow needs to reach it from the direction of c.
        // Walk b→c→d.
        let bc = b;
        // c == cd now
        let cd = oldc + bc;
        (cd, -bc)
    };
    [ab, -cb, cd, -ad]
}

fn generate_flows(split_count: u8, f: impl Fn(u8, &Index) -> f32) -> Flows {
    let mut flows = Flows::new((1, 1));
    for i in 0..split_count {
        flows = flows.divide();
        flows.stir(|index| f(i, index));
    }
    flows
}

/// Generates a flowing square array
pub fn generate(split_count: u8, f: impl Fn(u8, &Index) -> f32) -> FlowField {
    let flows = generate_flows(split_count, f);
    flows.to_vector_field()
}


fn extend(data: &[f32]) -> Vec<f32> {
    data.iter()
        .chain(data.iter())
        .map(|v| *v)
        .collect()
}


/// Generators of concrete flow fields
pub mod stirs {
    use super::Index;

    /// Really basic random integer generator.
    fn tausworthe(mut seed: usize) -> usize {
      seed ^= seed >> 13;
      seed ^= seed << 18;
      seed & 0x7fffffff
    }

    pub fn random_basic(iter: u8, index: &Index) -> f32 {
        0.001 / (tausworthe(iter as usize * index.x + index.y as usize) as f32 + 1.0)
    }

    pub fn const_one(_iter: u8, _index: &Index) -> f32 {
        1.0
    }
    
    pub fn const_one_edge(_iter: u8, index: Index) -> f32 {
        if index.x == 0 {
            0.0
        } else {
            1.0
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn is_flow_correct(surplus: &[f32], flows: &[f32]) -> bool {
        let flows = extend(flows);
        let eps = 10e-6;
        surplus.iter()
            .enumerate()
            .find(|(i, v)| (*v - flows[*i] + flows[i + 3]).abs() > eps)
            .is_none()
    }

    #[derive(Debug, PartialEq)]
    enum DivergenceResult {
        Zero,
        Present(f32, Index),
    }
    
    fn check_divergence(flows: &Flows) -> DivergenceResult {
        let eps = 10e-6;
        for index in flows.cells() {
            let divergence = flows.divergence_at(&index);
            if divergence.abs() > eps {
                println!("↓ {}", flows.from_lower_y(&index));
                println!("{} → ← {}", flows.from_lower_x(&index), flows.from_higher_x(&index));
                println!("↑ {}", flows.from_higher_y(&index));
                return DivergenceResult::Present(divergence, index);
            }
        }
        DivergenceResult::Zero
    }
    
    #[test]
    fn test_surplus_empty() {
        assert_eq!(
            split_flows(&extend(&[0.0, 0.0, 0.0, 0.0])),
            [0.0, 0.0, 0.0, 0.0]
        );
    }

    #[test]
    fn test_surplus_peaks() {
        assert_eq!(
            split_flows(&extend(&[1.0, -1.0, 1.0, -1.0])),
            [0.5, -0.5, 0.5, -0.5]
        );
    }
    
    #[test]
    fn test_surplus_straight() {
        assert_eq!(
            split_flows(&extend(&[1.0, 1.0, -1.0, -1.0])),
            [0.0, 1.0, 0.0, -1.0]
        );
    }

    #[test]
    fn test_surplus_straight2() {
        assert_eq!(
            split_flows(&extend(&[1.0, -1.0, -1.0, 1.0])),
            [1.0, 0.0, -1.0, 0.0]
        );
    }

    #[test]
    fn test_surplus_neighbors() {
        let surplus = [1.0, 1.0, -2.0, 0.0];
        assert!(is_flow_correct(&surplus, &split_flows(&extend(&surplus))));
        // Does not need to give the exact same answer, but this one is nice.
        assert_eq!(
            split_flows(&extend(&surplus)),
            [0.0, 1.0, -1.0, -1.0]
        );
    }

    // Does not need to give the exact same answer.
    #[test]
    fn test_surplus_neighbors2() {
        assert_eq!(
            split_flows(&extend(&[1.0, 1.0, 0.0, -2.0])),
            [0.0, 1.0, 1.0, -1.0]
        );
    }

    #[test]
    fn test_surplus_drain() {
        assert_eq!(
            split_flows(&extend(&[1.0, 0.5, -2.0, 0.5])),
            [0.5, 1.0, -1.0, -0.5]
        );
    }

    // Does not need to give the exact same answer.
    #[test]
    fn test_surplus_simple() {
        assert_eq!(
            split_flows(&extend(&[3.0, -3.0, 0.0, 0.0])),
            [2.0, -1.0, -1.0, -1.0]
        );
    }

    #[test]
    fn test_surplus_slope() {
        assert_eq!(
            split_flows(&extend(&[1.0, 0.0, -1.0, 0.0])),
            [0.5, 0.5, -0.5, -0.5]
        );
    }

    #[test]
    fn test_surplus_slope_shifted() {
        assert_eq!(
            split_flows(&extend(&[1.0, 0.0, -1.0, 0.0])),
            [0.5, 0.5, -0.5, -0.5]
        );
    }
    
    #[test]
    fn test_stir() {
        let mut flows = Flows::new((2, 2));
        flows.stir(|index| stirs::const_one(0, index));
        assert_eq!(
            Flows {
                x_flows: Array2::from(vec![[1.0, -1.0]]),
                y_flows: Array2::from(vec![[-1.0], [1.0]]),
            },
            flows,
        );
        assert_eq!(check_divergence(&flows), DivergenceResult::Zero);
    }
    
    #[test]
    fn test_fill_known() {
        let flows = Flows {
            x_flows: Array2::from(vec![[1.0, -1.0]]),
            y_flows: Array2::from(vec![[-1.0], [1.0]]),
        };
        let flows = flows.fill_known();
        assert_eq!(
            &flows,
            &Flows {
                x_flows: Array2::from(vec![
                    [0.0, 0.0, 0.0, 0.0],
                    [1.0, 1.0, -1.0, -1.0],
                    [0.0, 0.0, 0.0, 0.0],
                ]),
                y_flows: Array2::from(vec![
                    [0.0, -1.0, 0.0],
                    [0.0, -1.0, 0.0],
                    [0.0, 1.0, 0.0],
                    [0.0, 1.0, 0.0],
                ]),
            },
        );
        //assert_eq!(check_divergence(&flows), DivergenceResult::Zero);
    }

    #[test]
    fn test_surplus() {
        let flows = Flows {
            x_flows: Array2::from(vec![
                [0.0, 0.0, 0.0, 0.0],
                [1.0, 1.0, -1.0, -1.0],
                [0.0, 0.0, 0.0, 0.0],
            ]),
            y_flows: Array2::from(vec![
                [0.0, -1.0, 0.0],
                [0.0, -1.0, 0.0],
                [0.0, 1.0, 0.0],
                [0.0, 1.0, 0.0],
            ]),
        };
        assert_eq!(
            flows._surplus_at_square(&Index { x: 0, y: 0 }),
            [0.0, -1.0, 0.0, 1.0],
        );
    }

    #[test]
    fn test_fill_split() {
        let mut flows = Flows {
            x_flows: Array2::from(vec![
                [0.0, 0.0, 0.0, 0.0],
                [1.0, 1.0, -1.0, -1.0],
                [0.0, 0.0, 0.0, 0.0],
            ]),
            y_flows: Array2::from(vec![
                [0.0, -1.0, 0.0],
                [0.0, -1.0, 0.0],
                [0.0, 1.0, 0.0],
                [0.0, 1.0, 0.0],
            ]),
        };
        flows.fill_split();
        assert_eq!(check_divergence(&flows), DivergenceResult::Zero);
    }
    
    #[test]
    fn test_gen_ones() {
        let flows = generate_flows(2, stirs::const_one);
        assert_eq!(check_divergence(&flows), DivergenceResult::Zero);
    }

    #[test]
    fn test_gen_ones_big() {
        let flows = generate_flows(5, stirs::const_one);
        assert_eq!(check_divergence(&flows), DivergenceResult::Zero);
    }

    #[test]
    fn test_gen_basic() {
        let flows = generate_flows(2, stirs::random_basic);
        assert_eq!(check_divergence(&flows), DivergenceResult::Zero);
    }
    
    // Flow field related
    use ndarray::{ ArrayView2, s };
    use nalgebra::geometry::{ Point2, Rotation2 };
    
    
    use nalgebra::Normed;
    
    
    type FlowFieldSlice<'a> = ArrayView2<'a, Complex<f32>>;
    
    fn c(a: f32, b: f32) -> Complex<f32> {
        Complex::new(a, b)
    }
    
    /// Rotate by 45 degrees foo-wise.
    fn rot(p: Complex<f32>) -> Complex<f32> {
        let p = Point2::new(p.re, p.im);
        let o = Rotation2::new(std::f32::consts::TAU / 8.0).transform_point(&p);
        Complex::new(o.coords[0], o.coords[1])
    }
    
    /// Not to scale. We only want to compare to 0 anyway.
    /// Should be =0 for every piece, fluid does not compress
    fn divergence2_2(neighbors: FlowFieldSlice) -> f32 {
        // Rate of flow change ("derivative") over the row axis.
        // Maybe divide by 2. Not sure about signs.
        let dmatching // both axes increase together
            = rot(neighbors[(0, 0)]).im - rot(neighbors[(1, 1)]).im;
        let dopposite // both axes increase opposite
            = rot(neighbors[(0, 1)]).re - rot(neighbors[(1, 0)]).re;
        -(dmatching + dopposite) / c(1.0, 1.0).norm()
    }

    /// Checks each 2x2 for having any divergence
    fn divergence_check_squares(flows: FlowField) -> DivergenceResult {
        fn check_offset(sub: FlowFieldSlice) -> DivergenceResult {
            for slice in sub.windows((2, 2)) {
                let divergence = divergence2_2(slice);
                if divergence.abs() > 1e-6 {
                    println!("{:#?}", slice);
                    println!("div {}", divergence2_2(slice));
                    return DivergenceResult::Present(divergence, Index { x: 0, y: 0 })
                }
            }
            DivergenceResult::Zero
        }
        
        if let DivergenceResult::Present(f, i) = check_offset(flows.slice(s![.., ..])) {
            return DivergenceResult::Present(f, i);
        }
        if let DivergenceResult::Present(f, i) = check_offset(flows.slice(s![1.., ..])) {
            return DivergenceResult::Present(f, i);
        }
        if let DivergenceResult::Present(f, i) = check_offset(flows.slice(s![.., 1..])) {
            return DivergenceResult::Present(f, i);
        }
        if let DivergenceResult::Present(f, i) = check_offset(flows.slice(s![1.., 1..])) {
            return DivergenceResult::Present(f, i);
        }
        DivergenceResult::Zero
    }
    
    #[test]
    fn test_field_ones() {
        let flows = generate(2, stirs::const_one);
        assert_eq!(
            divergence_check_squares(flows),
            DivergenceResult::Zero,
        );
    }
    
    #[test]
    fn test_field_ones_big() {
        let flows = generate(5, stirs::const_one);
        assert_eq!(
            divergence_check_squares(flows),
            DivergenceResult::Zero,
        );
    }

    #[test]
    fn test_field_basic() {
        let flows = generate(2, stirs::const_one);
        assert_eq!(
            divergence_check_squares(flows),
            DivergenceResult::Zero,
        );
    }
    
    #[test]
    fn test_field_basic_big() {
        let flows = generate(5, stirs::const_one);
        assert_eq!(
            divergence_check_squares(flows),
            DivergenceResult::Zero,
        );
    }
    
}
