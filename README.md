Fluw simulation
=========

A lava lamp within a square. It gives a nice, patient flow, without getting stuck.

[Video](https://porcupinefactory.org/data/floow.webm)

I made it as eye candy for use in [Breedmatic](https://gitlab.com/dcz_self/breedmatic.git).

It includes a module for [Bevy](https://bevyengine.org/)

Running
----------

```
cd fluw
cargo run
```

Fluid
------

It's just a static vector field in a grid. Incompressible fluid.

Numerics
-----------

Okay, flakes will get stuck in walls on their first trip around. That's unavoidable. If fluid in any square in the grid flows towards the wall, it will get stuck in any simulation (without extra precautions).

Flakes will not follow the same path forever either. That's because they take discrete steps at every frame, and they move according to the flow at the departure point, not at the arrival point. That will push them towards the outside of circles. Not sure how to eliminate this.

The maths
-------------

The fun part of this project is generating a flow field that makes sense, and in a simple way. While it took more than the original estimate of 1 hour, it's still simple.

First rule of simple fluids: the amount of liquid coming into an area equals the amount coming out of it.

*[Divergence](https://en.wikipedia.org/wiki/Divergence) at each point == 0.*

Grid is generated from the top down: starting with a 1×1 array, then 2×2, 4×4, and so on. At each step, two operations are performed:

1. Split each cell into a 2×2 section while preserving divergence
2. Stir each 2×2 window clockwise.

Stirring preserves divergence by definition, as it only moves liquid in a circle.

Splitting is more complicated. Naively splitting the flow inside a grid cell into 4 identical mini-cells introduces problems on the edges. Instead, flow is not tracked in each cell, but between them. Each cell consists of 4 boundaries: left, right, top, and bottom.

Splitting such a cell gives 4 smaller cells: a known boundary to the outside, and 4 unknown flows between the cell. Within the square, it's easy to come up with a way to distribute flows without upsetting divergence.

After all splitting and stirring is done, the boundary representation is converted to a more traditional vector field, which gets used to guide flakes on the screen.

License
---------

AGPL version 3.0 or later.
